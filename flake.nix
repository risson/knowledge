{
  # name = "knowledge";
  description = "risson's knowledge base.";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    futils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, futils } @ inputs:
    let
      inherit (nixpkgs) lib;
      inherit (futils.lib) eachDefaultSystem defaultSystems;
      inherit (lib) recursiveUpdate genAttrs substring;

      nixpkgsFor = genAttrs defaultSystems (system: import nixpkgs {
        inherit system;
        overlays = [ self.overlay ];
      });

      version = "${substring 0 8 (self.lastModifiedDate or self.lastModified or "19700101")}_${self.shortRev or "dirty"}";
    in
    recursiveUpdate
    {
      overlay = final: prev: {
        knowledge = final.stdenvNoCC.mkDerivation {
          pname = "knowledge";
          inherit version;
          src = self;

          phases = [ "unpackPhase" "buildPhase" ];

          buildInputs = with final; [
            mdbook
          ];

          buildPhase = ''
            mkdir -p $out
            mdbook build --dest-dir $out
          '';

        };
      };
    }
    (eachDefaultSystem (system:
      let
        pkgs = nixpkgsFor.${system};
      in
      {
        devShell = pkgs.mkShell {
          inputsFrom = [ self.packages.${system}.knowledge ];
        };

        packages = {
          inherit (pkgs) knowledge;
        };

        defaultPackage = self.packages.${system}.knowledge;
      }
    ));
}
