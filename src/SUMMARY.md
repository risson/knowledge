# Summary

- [Radio](./radio/_index.md)
    - [Certification](./radio/certification.md)
    - [Techniques d'émission](./radio/tx-techniques/_index.md)
        - [Répéteurs](./radio/tx-techniques/repetears.md)
    - [Modes](./radio/modes/_index.md)
        - [APRS](./radio/modes/aprs.md)
        - [Hamnet](./radio/modes/hamnet.md)
    - [Communauté](./radio/community/_index.md)
        - [Serveurs Discord](./radio/community/discord.md)
        - [Youtube channels](./radio/community/youtube.md)
